package com.project.jin.haeng;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.JsonObject;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.ui.view.OAuthLoginButton;
import com.project.jin.haeng.common.ExtensionActivity;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.common.SettingManager;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ExtensionActivity implements GoogleApiClient.OnConnectionFailedListener {
    static final int RC_SIGN_IN = 8001;

    ImageButton btnFacebookLogin;
    ImageButton btnGoogleLogin;
    ImageButton btnNaverLogin;

    OAuthLoginButton mOAuthLoginButton;
    OAuthLogin mOAuthLoginModule;

    /* Google */
    FirebaseAuth mAuth;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInApi mGoogleSignInClient;

    /* Facebook */
    CallbackManager callbackManager;
    LoginButton loginFacebookButton; // 페이스북 기본 제공 버튼. 암시적으로 작동

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkAlreadyLogin();

        btnFacebookLogin = findViewById(R.id.btn_login_facebook);
        btnGoogleLogin = findViewById(R.id.btn_login_google);
        btnNaverLogin = findViewById(R.id.btn_login_naver);


//        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loginFacebookButton.performClick();
//            }
//        });
//
//        btnGoogleLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startGoogleLogin();
//            }
//        });
//        initFacebookLogin();

        initNaverLogin();
        btnNaverLogin.setOnClickListener(view -> mOAuthLoginButton.performClick());
    }

    private void startNaverLogin() {
        mOAuthLoginModule.startOauthLoginActivity(LoginActivity.this, mOAuthLoginHandler);
    }

    private void initNaverLogin() {
        mOAuthLoginButton = (OAuthLoginButton) findViewById(R.id.buttonOAuthLoginImg);
        mOAuthLoginButton.setOAuthLoginHandler(mOAuthLoginHandler);

        mOAuthLoginModule = OAuthLogin.getInstance();
        mOAuthLoginModule.init(
                LoginActivity.this
                ,"jbtWG1CGn63ujCOUQJI7"
                ,"Tsju_9uvmN"
                ,"행_Haeng"
        );
    }

    private void checkAlreadyLogin() {
        SettingManager setting = HaengApplication.getSettingManager();
        String userId = setting.getUserId();
        if (!userId.equals("")) {
            successToLogin(userId);
        }
    }

    private void successToLogin(String userId) {
        SettingManager setting = HaengApplication.getSettingManager();
        setting.setUserId(userId);

        sendToUserData();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        finish();
    }

    private void sendToUserData() {
//        ID, NAME, AGE, GENDER
        SettingManager setting = HaengApplication.getSettingManager();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UID", setting.getUserId());
        jsonObject.addProperty("NAME", setting.getUserName());
        jsonObject.addProperty("AGE", setting.getUserAge());
        jsonObject.addProperty("GENDER", setting.getUserSex());
        jsonObject.addProperty("IMAGE", setting.getUserImage());


        RetrofitAPI.getInstance().createHaengService(Request.class)
                .sendUserData(jsonObject)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        toast("유저데이터 저장 성공");
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                    }
                });
    }

    private void failToLogin() {
        toast("로그인에 실패했습니다.");
    }

    private void initFacebookLogin() {
        /* 페이스북 콜백 매니저 */
        callbackManager = CallbackManager.Factory.create();

        /* 로그인 초기화 */
        loginFacebookButton = (LoginButton) findViewById(R.id.btn_facebook_login_hidden);
        loginFacebookButton.setReadPermissions("email");

        // Callback registration
        loginFacebookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String userId = loginResult.getAccessToken().getUserId();
                successToLogin(userId);
            }

            @Override
            public void onCancel() {
                failToLogin();
            }

            @Override
            public void onError(FacebookException exception) {
                failToLogin();
            }
        });
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        mAuth = FirebaseAuth.getInstance();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String userId = mAuth.getCurrentUser().getUid();
                            successToLogin(userId);
                        } else {
                            failToLogin();
                        }
                    }
                });
    }

    void startGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = Auth.GoogleSignInApi;
        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
        .enableAutoManage(this, this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        .build();

        Intent signInIntent = mGoogleSignInClient.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                failToLogin();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        failToLogin();
    }

    private void getNaverProfileInfo(String accessToken) {
        HaengApplication.getSettingManager().setNaverAccessToken(accessToken);

        RetrofitAPI.getInstance().createNaverService(Request.class)
                .getNaverClientInfo()
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        JsonObject result = response.body();
                        JsonObject json = result.get("response").getAsJsonObject();
                        String age = json.get("age").getAsString();
                        String gender = json.get("gender").getAsString();
                        String birth = json.get("birthday").getAsString();
                        String email = json.get("email").getAsString();
                        String name = json.get("name").getAsString();
                        String id = json.get("id").getAsString();
                        String image = json.get("profile_image").getAsString();

                        SettingManager setting = HaengApplication.getSettingManager();
                        setting.setUserName(name);
                        setting.setUserAge(age);
                        setting.setUserSex(gender);
                        setting.setUserBirth(birth);
                        setting.setUserEmail(email);
                        setting.setUserImage(image);

                        successToLogin(id);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                    }
                });
    }


    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            Context mContext = LoginActivity.this;

            if (success) {
                String accessToken = mOAuthLoginModule.getAccessToken(mContext);
                String refreshToken = mOAuthLoginModule.getRefreshToken(mContext);
                long expiresAt = mOAuthLoginModule.getExpiresAt(mContext);
                String tokenType = mOAuthLoginModule.getTokenType(mContext);

                SettingManager setting = HaengApplication.getSettingManager();
                setting.setNaverAccessToken(accessToken);
                getNaverProfileInfo(accessToken);
            } else {
                String errorCode = mOAuthLoginModule.getLastErrorCode(mContext).getCode();
                String errorDesc = mOAuthLoginModule.getLastErrorDesc(mContext);
                Toast.makeText(mContext, "errorCode:" + errorCode
                        + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
            }
        };
    };

}
