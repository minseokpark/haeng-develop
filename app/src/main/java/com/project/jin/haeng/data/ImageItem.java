package com.project.jin.haeng.data;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minseok on 2018. 8. 21..
 * haeng-develop.
 *
 * Picture Image Entity in the PictureFragment.
 */
public class ImageItem extends RealmObject implements Serializable {
    @PrimaryKey public int index;
    public String userId;
    public String userName;
    public String userImg;
    public String location;
    public String date;
    public String content;

    public String localUrl;
    public String remoteUrl;

    // 0: 일반 사진, 1: 이벤트
    public Boolean isEvent = false;
}
