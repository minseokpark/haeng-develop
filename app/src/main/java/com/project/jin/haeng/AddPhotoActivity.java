package com.project.jin.haeng;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.project.jin.haeng.common.ExtensionActivity;
import com.project.jin.haeng.data.ImageItem;

import io.realm.Realm;
import io.realm.internal.IOException;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by jin on 2018-06-10.
 */

public class AddPhotoActivity extends ExtensionActivity {
    static final int CAMERA_REQUEST_CODE = 0001;
    static final int GALLERY_REQUEST_CODE = 0002;

    static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1000;

    String[] needPermisisons = {READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE};

    ImageView imgPicture;

    Button btnUpload, btnCancel;
    Button btnAddPhoto;
    EditText editContent;
    EditText editLocation;
    CheckBox checkBox;

    String mImgPath;
    Uri picture = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);

        imgPicture = findViewById(R.id.img_uploaded_picture);
        editLocation = findViewById(R.id.edit_location);
        editContent = findViewById(R.id.edit_contents);
        btnUpload = findViewById(R.id.btnUpload);
        btnCancel = findViewById(R.id.btnCancel);
        btnAddPhoto = findViewById(R.id.addPhoto);
        btnAddPhoto.setOnClickListener(view -> {

            if (!checkPermission()) {
                requestPermissions();
                return;
            }

            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        });

        btnUpload.setOnClickListener(view -> {
            sendPicture(mImgPath);
            finish();
        });

        btnCancel.setOnClickListener(view -> {
//                Intent intent = new Intent(getApplicationContext(),PictureActivity.class);
//                startActivity(intent);
            finish();
        });

        checkBox = findViewById(R.id.checkbox_event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {

        } else if (requestCode == GALLERY_REQUEST_CODE) {
            Log.d("Picture", "Try to get picture");

            if (data != null) {
                getPicture(data.getData()); //갤러리에서 가져오기
            }
        }
    }

    /**
     * 서버에 이미지를 업로드
     * @param imgUri
     */
    private void sendPicture(String imgUri) {
        if (imgUri == null) return;
//
//        SettingManager setting = HaengApplication.getSettingManager();
//
//        JsonObject json = new JsonObject();
//        json.addProperty("UID", setting.getUserId());
//        json.addProperty("EVENT", checkBox.isChecked());
//        json.addProperty("NAME", setting.getUserName());
//        json.addProperty("CONTENTS", editContent.getText().toString());
//        json.addProperty("LOCATION", editLocation.getText().toString());
////        json.addProperty("PHOTO", String.valueOf(imgUri));
//
//        File file = new File(mImgPath);
//        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//        MultipartBody.Part multipartBodyFile = MultipartBody.Part.createFormData("imageFile[]", file.getName(), requestBody);
//        ArrayList<MultipartBody.Part> tempList = new ArrayList<>();
//        tempList.add(multipartBodyFile);
//
//        RetrofitAPI.getInstance().createHaengService(Request.class)
//                .addPhoto(json)
//                .enqueue(new Callback<JsonObject>() {
//                    @Override
//                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                        toast("성공");
//                    }
//
//                    @Override
//                    public void onFailure(Call<JsonObject> call, Throwable t) {
//                        toast("실패");
//                    }
//                });

//        imgUri;


        Realm mRealm = Realm.getDefaultInstance();
        int size = mRealm.where(ImageItem.class).findAll().size();
        mRealm.beginTransaction();
        ImageItem item = mRealm.createObject(ImageItem.class, size);
        item.remoteUrl = mImgPath;
        item.content = editContent.getText().toString();
        item.location = editLocation.getText().toString();
        item.isEvent = checkBox.isChecked();
        mRealm.commitTransaction();
    }

    private void getPicture(Uri imgUri) {
        if (imgUri == null) return;

        String imagePath = getRealPathFromURI(imgUri); // path 경로
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
//        int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//        int exifDegree = exifOrientationToDegrees(exifOrientation);

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);//경로를 통해 비트맵으로 전환
//        imgPicture.setImageBitmap(bitmap);//이미지 뷰에 비트맵 넣기

        Glide.with(this).load(imagePath).into(imgPicture);

        mImgPath = imagePath;
//        picture = bitmap;
    }

    private String getRealPathFromURI(Uri contentUri) {
        int column_index=0;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if(cursor.moveToFirst()){
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        }

        return cursor.getString(column_index);
    }

    private int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private Bitmap rotate(Bitmap src, float degree) {
        // Matrix 객체 생성
        Matrix matrix = new Matrix();
        // 회전 각도 셋팅
        matrix.postRotate(degree);
        // 이미지와 Matrix 를 셋팅해서 Bitmap 객체 생성
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(),
                src.getHeight(), matrix, true);
    }

    private boolean checkPermission() {
        for (String permission : needPermisisons) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, needPermisisons,
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
            requestStoragePermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length == 0) {
            } else {
                btnAddPhoto.performClick();
            }
        }
    }
}
