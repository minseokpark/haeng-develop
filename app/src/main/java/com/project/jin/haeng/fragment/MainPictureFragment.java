package com.project.jin.haeng.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.project.jin.haeng.AddPhotoActivity;
import com.project.jin.haeng.MainActivity;
import com.project.jin.haeng.R;
import com.project.jin.haeng.common.ExtensionFragment;
import com.project.jin.haeng.common.GlobalUtils;
import com.project.jin.haeng.data.ImageItem;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPictureFragment extends ExtensionFragment {
    LinearLayout mContainer;
    ImageButton btnAdd;
    RecyclerView recyclerView;

    PicturesAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        addList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (LinearLayout) inflater.inflate(R.layout.fragment_picture, container, false);
            recyclerView = mContainer.findViewById(R.id.rv_container);
            btnAdd = mContainer.findViewById(R.id.btnAdd);

            btnAdd.setOnClickListener(view -> {
                Intent intent = new Intent(getActivity(), AddPhotoActivity.class);
                startActivity(intent);
            });

            mAdapter = new PicturesAdapter(getActivity(), new ArrayList<>(), new PictureCallback() {
                @Override
                public void onClickPicture(ImageItem item) {
//                    toast("" + item.index);
                    ((MainActivity) getActivity()).showDetailPicture(item);
                }
            });
            recyclerView.setAdapter(mAdapter);
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
            mAdapter.notifyDataSetChanged();
        }

        mAdapter = new PicturesAdapter(getActivity(), new ArrayList<>(), item -> ((MainActivity) getActivity()).showDetailPicture(item));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mAdapter.notifyDataSetChanged();

        return mContainer;
    }

    private void addList() {
        JsonObject jsonObject = new JsonObject();
        startProgress();
        RetrofitAPI.getInstance().createHaengService(Request.class)
                .getPhotos(jsonObject)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        endProgress();

                        ArrayList<ImageItem> imageList = new ArrayList<>();
                        JsonArray photos = response.body().get("response").getAsJsonArray();
                        for (JsonElement item : photos) {
                            JsonObject json = item.getAsJsonObject();

                            ImageItem img = new ImageItem();
                            img.index = json.get("NO").getAsInt();
                            img.userId = json.get("UID").getAsString();
                            img.userName = json.get("NAME").getAsString();
                            img.isEvent = json.get("EVENT").getAsInt() == 1;
                            img.content = json.get("CONTENTS").getAsString();
                            img.location = json.get("LOCATION").getAsString();
                            img.date = json.get("UPLOAD").getAsString();
                            img.remoteUrl = json.get("FILENAME").getAsString();
                            img.userImg = json.get("IMAGE").getAsString();

                            imageList.add(img);
                        }

                        Realm mRealm = Realm.getDefaultInstance();
                        RealmResults<ImageItem> results = mRealm.where(ImageItem.class).findAll();
                        imageList.addAll(results);

                        mAdapter.refreshList(imageList);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        endProgress();
                        toast("실패!");
                    }
                });
    }

    public static class PicturesAdapter extends RecyclerView.Adapter<PictureViewHolder> {
        Context mContext;
        List<ImageItem> mList;
        PictureCallback mCallback;

        public PicturesAdapter(Context context, List<ImageItem> list, PictureCallback callback) {
            this.mContext = context;
            this.mList = list;
            this.mCallback = callback;
        }

        public void refreshList(List mList) {
            this.mList = mList;
            notifyDataSetChanged();
        }

        @Override
        public PictureViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = ConstraintLayout.inflate(mContext, R.layout.item_picture, null);

            v.setLayoutParams(new ConstraintLayout.LayoutParams(GlobalUtils.dpTopx(100), GlobalUtils.dpTopx(100)));

            return new PictureViewHolder(v);
        }

        @Override
        public void onBindViewHolder(PictureViewHolder holder, final int position) {
            ImageView imgview = holder.imgView;

            ImageItem item = mList.get(position);

//            if (item.remoteUrl == null) {
//                Glide.with(mContext).load(item.localUrl).into(imgview);
//            } else {
            if (item.remoteUrl.contains("/storage/")) {
                Glide.with(mContext).load(item.remoteUrl).into(imgview);
            } else {
                Glide.with(mContext).load(GlobalUtils.getServerUrl() + item.remoteUrl).into(imgview);
                Log.d("URL", GlobalUtils.getServerUrl() + item.remoteUrl);
            }

//            }

            imgview.setOnClickListener(view -> mCallback.onClickPicture(mList.get(position)));
        }

        @Override
        public int getItemCount() { return mList.size(); }
    }

    public static class PictureViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView imgView;

        public PictureViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            bind();
        }

        public void bind() {
            imgView = itemView.findViewById(R.id.img_picture);
        }
    }

    interface PictureCallback {
        void onClickPicture(ImageItem item);
    }
}
