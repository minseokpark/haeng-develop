package com.project.jin.haeng.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.project.jin.haeng.R;
import com.project.jin.haeng.data.MessageItem;

import java.util.ArrayList;

/**
 * Created by minseok on 2018. 8. 16..
 * haeng-develop.
 */
public class ChattingAdapter extends RecyclerView.Adapter<ChattingAdapter.MessageViewHolder> {
    ArrayList<MessageItem> mList = new ArrayList<>();
    Context mContext;

    public ChattingAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.item_chat, parent, false);
        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        holder.tvMessage.setText(mList.get(position).msg);
        holder.tvName.setText(mList.get(position).sender);
    }

    @Override
    public int getItemCount() { return mList.size(); }

    public void addMessage(MessageItem msg) {
        mList.add(msg);
        notifyDataSetChanged();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        View itemView;

        TextView tvMessage;
        TextView tvName;
        ImageView ivPic;

        public MessageViewHolder(View view) {
            super(view);

            this.itemView = view;
            bind();
        }

        public void bind() {
            tvName = itemView.findViewById(R.id.ch_name);
            tvMessage = itemView.findViewById(R.id.ch_contents);
        }
    }
}
