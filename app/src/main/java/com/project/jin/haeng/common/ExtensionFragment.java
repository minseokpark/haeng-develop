package com.project.jin.haeng.common;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.project.jin.haeng.R;

import io.realm.Realm;

/**
 * Created by minseok on 2018. 8. 15..
 * haeng-develop.
 */
public class ExtensionFragment extends android.support.v4.app.Fragment{

    private ProgressDialog progressDialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Realm.init(getContext());

        createProgress();
    }

    private void createProgress() {
        progressDialog = new ProgressDialog(getActivity(), R.style.LoadingProgressStyle);
        progressDialog.setCancelable(false);
    }

    /**
     * 프로그레스 시작
     */
    public void startProgress() {
        Message msg = new Message();
        progressHandler.sendMessage(msg);
    }

    /**
     * 프로그레스 종료
     */
    public void endProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    public Handler progressHandler = new Handler(msg -> {
        if (progressDialog != null) {
            try {
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
                progressDialog.setMessage(getString(R.string.loading_dialog_content));
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) { }
        }

        return false;
    });

    public void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
