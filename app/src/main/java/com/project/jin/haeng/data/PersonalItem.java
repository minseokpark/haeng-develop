package com.project.jin.haeng.data;

/**
 * Created by jin on 2018-06-07.
 */

public class PersonalItem {
    private String location;
    private String period;
    private String interest;
    private String contents;

    public void setLocation(String location) {
        this.location = location;
    }
    public void setPeriod(String period){
        this.period = period;
    }
    public void setInterest(String interest){
        this.interest = interest;
    }
    public void setContents(String contents){
        this.contents = contents;
    }

    public String getLocation() {
        return location;
    }
    public String getPeriod() {
        return period;
    }
    public String getInterest() {
        return interest;
    }
    public String getContents() {
        return contents;
    }
}
