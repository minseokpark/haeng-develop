package com.project.jin.haeng.adapter;

import com.project.jin.haeng.data.ChatItem;

/**
 * Created by minseok on 2018. 8. 18..
 * haeng-develop.
 */
public interface ChattingAdapterView {
    void enterChatting(ChatItem item);
}
