package com.project.jin.haeng.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jin on 2018-05-24.
 */

public class SearchItem extends RealmObject {

    @PrimaryKey
    private int id = 0;
    private int iconResId;
    private String name;
    private String uid;
    private String location;
    private String period;
    private String contents;
    private String interest;
    private String userImg;

    public void setId(int id) { this.id = id; }
    public void setUid(String uid) { this.uid = uid; }
    public void setIconResId(int icon) {
        this.iconResId = icon;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public void setPeriod(String period){
        this.period = period;
    }
    public void setContents(String contents){
        this.contents = contents;
    }
    public void setInterest(String interest) { this.interest = interest; }

    public int getId() { return id; }
    public String getUid() { return uid; }
    public int getIconResId() {
        return iconResId;
    }
    public String getName() {
        return name;
    }
    public String getLocation() {
        return location;
    }
    public String getPeriod() {
        return period;
    }
    public String getContents() {
        return contents;
    }
    public String getInterest() { return interest; }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }
}
