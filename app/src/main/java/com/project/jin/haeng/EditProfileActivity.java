package com.project.jin.haeng;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.project.jin.haeng.common.ExtensionActivity;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.common.SettingManager;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jin on 2018-06-10.
 */

public class EditProfileActivity extends ExtensionActivity {
    Button btnSave, btnCancel;

    EditText dlgLocation;
    EditText dlgIntro;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        btnSave = (Button)findViewById(R.id.btnSave);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        dlgLocation = findViewById(R.id.dlgLocation);
        dlgIntro = findViewById(R.id.dlgIntro);

        btnSave.setOnClickListener(view -> {
            SettingManager setting = HaengApplication.getSettingManager();

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("UID", setting.getUserId());
            jsonObject.addProperty("NAME", setting.getUserName());
            jsonObject.addProperty("LOCATION", dlgLocation.getText().toString());
            jsonObject.addProperty("INTRO", dlgIntro.getText().toString());

            RetrofitAPI.getInstance().createHaengService(Request.class)
                    .updateProfile(jsonObject)
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            setting.setUserAddress(dlgLocation.getText().toString());
                            setting.setUserIntroduce(dlgIntro.getText().toString());

                            finish();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            finish();
                        }
                    });
        });
        btnCancel.setOnClickListener(view -> finish());

    }
}
