package com.project.jin.haeng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.jin.haeng.R;
import com.project.jin.haeng.data.ChatItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jin on 2018-05-24.
 */

public class ChatListAdapter extends BaseAdapter{

    /* 아이템을 세트로 담기 위한 어레이 */
    private ArrayList<ChatItem> cItems = new ArrayList<>();
    private ChattingAdapterView mView;
    private Context mContext;

    public ChatListAdapter(Context context, ChattingAdapterView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public int getCount() {
        return cItems.size();
    }

    @Override
    public ChatItem getItem(int position) {
        return cItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        /* 'listview_custom' Layout을 inflate하여 convertView 참조 획득 */
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_chat, parent, false);
        }

        /* 'listview_custom'에 정의된 위젯에 대한 참조 획득 */
        ImageView ch_img = convertView.findViewById(R.id.ch_img);
        TextView ch_name = convertView.findViewById(R.id.ch_name);
        TextView ch_contents = convertView.findViewById(R.id.ch_contents);

        /* 각 리스트에 뿌려줄 아이템을 받아오는데 mMyItem 재활용 */
        final ChatItem myItem = getItem(position);

        /* 각 위젯에 세팅된 아이템을 뿌려준다 */
//        ch_img.setImageDrawable(ContextCompat.getDrawable(mContext, myItem.getIconResId()));
        ch_name.setText(myItem.getName());
        ch_contents.setText(myItem.getContents());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.enterChatting(myItem);
            }
        });

        return convertView;
    }

    public void addItem(int imgRes, String name, String contents) {

        ChatItem cItem = new ChatItem();
        cItem.setIconResId(imgRes);
        cItem.setName(name);
        cItem.setContents(contents);

        /* mItems에 MyItem을 추가한다. */
        cItems.add(cItem);
    }

    public void addItem(ChatItem item) { cItems.add(item); }

    public void addItems(List<ChatItem> items) {
        cItems.addAll(items);
    }
}