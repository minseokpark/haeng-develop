package com.project.jin.haeng.common;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import io.fabric.sdk.android.Fabric;

/**
 * Created by minseok on 2018. 8. 15..
 * haeng-develop.
 */
public class HaengApplication extends Application{

    static Context mContext;
    static SettingManager mSettingManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());

        mContext = getApplicationContext();
    }

    public static SettingManager getSettingManager() {
        if (mSettingManager == null) {
            mSettingManager = new SettingManager(mContext);
        }

        if (!mSettingManager.isLoaded) {
            mSettingManager.loadSettingValue();
        }

        return mSettingManager;
    }
}
