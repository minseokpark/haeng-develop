package com.project.jin.haeng.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jin on 2018-05-24.
 */

public class ChatItem extends RealmObject{

//    private Drawable icon;
    private int iconResId;
    @PrimaryKey private String name;
    private String contents;

//    public Drawable getIcon() {
//        return icon;
//    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

}
