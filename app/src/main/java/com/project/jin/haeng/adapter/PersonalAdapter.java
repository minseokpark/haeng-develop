package com.project.jin.haeng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.project.jin.haeng.R;
import com.project.jin.haeng.data.PersonalItem;

import java.util.ArrayList;

/**
 * Created by jin on 2018-06-07.
 */

public class PersonalAdapter extends BaseAdapter {

    private ArrayList<PersonalItem> pItems = new ArrayList<>();

    @Override
    public int getCount() {
        return pItems.size();
    }

    @Override
    public PersonalItem getItem(int position) {
        return pItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_travel_history, parent, false);
        }

        TextView location = (TextView)convertView.findViewById(R.id.location);
        TextView period = (TextView)convertView.findViewById(R.id.period);
        TextView interest = (TextView)convertView.findViewById(R.id.interest);
        TextView contents = (TextView)convertView.findViewById(R.id.contents);

        PersonalItem myItem = getItem(position);
        location.setText(myItem.getLocation());
        period.setText(myItem.getPeriod());
        interest.setText(myItem.getInterest());
        contents.setText(myItem.getContents());

        return convertView;
    }
    public void addItem(String location, String period, String interest, String contents) {

        PersonalItem pItem = new PersonalItem();

        pItem.setLocation(location);
        pItem.setPeriod(period);
        pItem.setInterest(interest);
        pItem.setContents(contents);

        pItems.add(pItem);
    }
}
