package com.project.jin.haeng.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by minseok on 09/10/2018.
 * haeng-develop.
 */
public interface Request {
    @GET("users/{user}/repos")
    Call<JsonObject> getNaverAccesToken(@Path("user") String user);

    @GET("nid/me")
    Call<JsonObject> getNaverClientInfo();

    /* Remote DB */
    @POST("AddUser.php")
    Call<JsonObject> sendUserData(@Body JsonObject jsonObject);

    @POST("AddBoard.php")
    Call<JsonObject> addUserBoard(@Body JsonObject jsonObject);

    @POST("SelectGallery.php")
    Call<JsonObject> getPhotos(@Body JsonObject jsonObject);

    @POST("AddGallery.php")
    Call<JsonObject> addPhoto(@Body JsonObject jsonObject);

    @POST("SelectEvent.php")
    Call<JsonObject> checkEvent(@Body JsonObject jsonObject);

    @POST("SelectMyBoard.php")
    Call<JsonObject> getMyBoard(@Body JsonObject jsonObject);

    @POST("SelectMyGallery.php")
    Call<JsonObject> getMyPictures(@Body JsonObject jsonObject);

    @POST("UpdateProfile.php")
    Call<JsonObject> updateProfile(@Body JsonObject jsonObject);

    /* 동행 조회 */
    @GET("SelectBoard.php")
    Call<JsonObject> getCompanions();
}
