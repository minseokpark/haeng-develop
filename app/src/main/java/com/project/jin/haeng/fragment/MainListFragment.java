package com.project.jin.haeng.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.project.jin.haeng.ChatroomActivity;
import com.project.jin.haeng.EventDialog;
import com.project.jin.haeng.NewCompanyActivity;
import com.project.jin.haeng.R;
import com.project.jin.haeng.adapter.SearchAdapter;
import com.project.jin.haeng.common.ExtensionFragment;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.data.ChatItem;
import com.project.jin.haeng.data.SearchItem;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.project.jin.haeng.MainActivity.btnChat;

public class MainListFragment extends ExtensionFragment {
    LinearLayout mContainer;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView sListView;
    SearchAdapter mMyAdapter;

    ImageButton btnAlarm;
    ImageView listBtn;
    Button btnFilter;
    Button btnSchedule;
    EditText editTravelTo;
    View dlgView;

    String intersResult = "";


    RadioButton filter_1;
    RadioButton filter_2;
    RadioButton filter_3;
    RadioButton filter_4;
    RadioButton filter_5;
    RadioButton filter_6;
    RadioButton filter_7;
    RadioButton filter_8;
    RadioButton filter_9;

    boolean isEditInSeconds = false;
    Timer mTimer;
    TimerTask mTimerTask;

    public MainListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (LinearLayout) inflater.inflate(R.layout.fragment_main_list, container, false);

            sListView = (ListView) mContainer.findViewById(R.id.listView);
            listBtn = (ImageView) mContainer.findViewById(R.id.listBtn);
            btnAlarm = (ImageButton) mContainer.findViewById(R.id.btnAlarm);
            btnFilter = (Button) mContainer.findViewById(R.id.btnFilter);
            editTravelTo = (EditText) mContainer.findViewById(R.id.txt_travelTo);
            btnSchedule = (Button) mContainer.findViewById(R.id.btnSchedule);

            listBtn.setOnClickListener(view -> {
                Intent intent = new Intent(getActivity(), NewCompanyActivity.class);
                startActivity(intent);
            });

            btnAlarm.setOnClickListener(view -> {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("UID", HaengApplication.getSettingManager().getUserId());
                RetrofitAPI.getInstance().createHaengService(Request.class)
                        .checkEvent(jsonObject)
                        .enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                JsonObject result = response.body();
                                String imagePath = result.get("response").getAsJsonArray().get(0).getAsJsonObject().get("PATH").getAsString();
                                if (imagePath.isEmpty()) {
                                    toast("해당되는 이벤트가 없습니다.");
                                } else {
//                                    AlertDialog.Builder dlg = new AlertDialog.Builder(getActivity());
//////                                    dlg.setTitle("돌발 이벤트!!!");
//////                                    dlg.setMessage("사용자 님은 현재 인기지역 '파리'에 있습니다.\n\n" + "미션 : 에펠탑 앞에서 2명이상의 사람들과 사진 찍기\n" +
//////                                            "제한시간 : 1시간\n" + "상품 : 스타벅스 아메리카노 쿠폰\n" + "참여방법 : 사진-글올리기(+)-이벤트사진-돌발이벤트 체크");
//////                                    dlg.setPositiveButton("확인", null);
//////                                    dlg.show();

                                    EventDialog dialog = new EventDialog(getActivity(), imagePath);
                                    dialog.show();
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

            });
            btnFilter.setOnClickListener(view -> {
                dlgView = (View)View.inflate(getActivity(), R.layout.dialog_filter,null);
                filter_1 = dlgView.findViewById(R.id.filter_1);
                filter_1.setOnClickListener(view1 -> selectInterest(view1));
                filter_2 = dlgView.findViewById(R.id.filter_2);
                filter_2.setOnClickListener(view2 -> selectInterest(view2));
                filter_3 = dlgView.findViewById(R.id.filter_3);
                filter_3.setOnClickListener(view3 -> selectInterest(view3));
                filter_4 = dlgView.findViewById(R.id.filter_4);
                filter_4.setOnClickListener(view4 -> selectInterest(view4));
                filter_5 = dlgView.findViewById(R.id.filter_5);
                filter_5.setOnClickListener(view5 -> selectInterest(view5));
                filter_6 = dlgView.findViewById(R.id.filter_6);
                filter_6.setOnClickListener(view6 -> selectInterest(view6));
                filter_7 = dlgView.findViewById(R.id.filter_7);
                filter_7.setOnClickListener(view7 -> selectInterest(view7));
                filter_8 = dlgView.findViewById(R.id.filter_8);
                filter_8.setOnClickListener(view8 -> selectInterest(view8));
                filter_9 = dlgView.findViewById(R.id.filter_9);
                filter_9.setOnClickListener(view9 -> selectInterest(view9));
                AlertDialog.Builder dlg = new AlertDialog.Builder(getActivity());
                dlg.setTitle("관심사 선택");
                dlg.setView(dlgView);
                dlg.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mMyAdapter.filterCategory(intersResult);
                    }
                });
                dlg.setNegativeButton("취소",null);
                dlg.show();
            });

            /* 3초 이내로 텍스트 변경 없을시 검색 API 사용 */
            editTravelTo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    isEditInSeconds = true;
                }

                @Override
                public void afterTextChanged(Editable editable) {
//                    if (mTimer != null && isEditInSeconds) {
//                        mTimer.cancel();
//                    }
//
//                    mTimer = new Timer();
//                    mTimer.schedule(getNewTimerTask(), 3000);

                    mMyAdapter.filterDestination(editTravelTo.getText().toString());
                }
            });

            btnSchedule.setOnClickListener(view -> {
                DatePickerDialog startDate;
                final DatePickerDialog endDate;

                Calendar today = Calendar.getInstance();
                int todayMonth = today.get(Calendar.MONTH);
                int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
                int todayYear = today.get(Calendar.YEAR);

                endDate = new DatePickerDialog(getActivity(),
                        (view1, year, month, day) -> {
                            Calendar c = Calendar.getInstance();
                            c.set(year, month, day);

                            String departmentDate = btnSchedule.getText().toString();

                            try {
                                String arriveDate = new SimpleDateFormat("yyyy.MM.dd").format(c.getTime());

                                Date departDate = new SimpleDateFormat("yyyy.MM.dd-").parse(departmentDate);
                                Date arrive = new SimpleDateFormat("yyyy.MM.dd").parse(arriveDate);

                                if (departDate.before(arrive)) {
                                    btnSchedule.setText(departmentDate + arriveDate);
                                    mMyAdapter.filterSchedule(departmentDate + arriveDate);
                                } else {
                                    toast("기간 설정이 잘못되었습니다.");
                                    btnSchedule.setText("일정을 입력하세요");
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        }, todayYear, todayMonth, todayDayOfMonth);
                endDate.setTitle("도착 일정");
                endDate.setCancelable(false);

                startDate = new DatePickerDialog(getActivity(),
                        (view12, year, month, day) -> {
                            Calendar c = Calendar.getInstance();
                            c.set(year, month, day);
                            String departmentDate = new SimpleDateFormat("yyyy.MM.dd-").format(c.getTime());

                            btnSchedule.setText(departmentDate);
                            endDate.show();
                        }, todayYear, todayMonth, todayDayOfMonth);
                startDate.setTitle("출발 일정");
                startDate.setCancelable(false);
                startDate.show();
            });
        }

        return mContainer;
    }

    @Override
    public void onResume() {
        super.onResume();

        /* 아이템 추가 및 어댑터 등록 */
        initAdapter();
        dataSetting();
    }

    private void dataSetting() {
        ArrayList<SearchItem> list = new ArrayList<>();

        startProgress();
        RetrofitAPI.getInstance().createHaengService(Request.class)
//                .getCompanions(jsonObject)
                .getCompanions()
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        endProgress();

                        JsonArray jsonArray = response.body().get("response").getAsJsonArray();

                        for (JsonElement je : jsonArray) {
                            JsonObject post = je.getAsJsonObject();

                            SearchItem item = new SearchItem();
                            item.setContents(post.get("CONTEXT").getAsString());
                            item.setName(post.get("NAME").getAsString());
                            item.setUid(post.get("UID").getAsString());
                            item.setUserImg(post.get("IMAGE").getAsString());
                            item.setLocation(post.get("DESTINATION").getAsString());
                            item.setInterest(post.get("INTREST").getAsString());

                            String period = post.get("STARTDATE").getAsString()
                                    + " - " + post.get("ENDDATE").getAsString();
                            item.setPeriod(period);

                            String rawInt = post.get("NO").getAsString();
                            int index = Integer.parseInt(rawInt);
                            item.setId(index);

                            list.add(item);
                        }

                        mMyAdapter.addAll(list);
                        mMyAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        endProgress();
                        getActivity().runOnUiThread(() -> toast("실패하였습니다!"));
                        t.printStackTrace();
                        Log.d("HAENG", t.getMessage());
                    }
                });
//        {
//            "NO": "85",
//                "UID": "김성진",
//                "DESTINATION": "서울",
//                "CONTEXT": "왜...",
//                "STARTDATE": "2018-01-01",
//                "ENDDATE": "2018-01-02",
//                "INTREST": "0"
//        },
    }

    private TimerTask getNewTimerTask() {
        return new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(() -> {
                    toast("지금 딱 리퀘스트를 딱");
                    isEditInSeconds = false;
                });
            }
        };
    }

    private void initAdapter() {
        mMyAdapter = new SearchAdapter(getActivity(), item -> {
            Intent intent = new Intent(getActivity(), ChatroomActivity.class);
            intent.putExtra("name", item.getName());
            startActivity(intent);

            makeChat(item);

            /* 메인액티비티 채팅 탭으로 이동 */
            btnChat.performClick();
        });
        sListView.setAdapter(mMyAdapter);
    }

    private void makeChat(ChatItem item) {
        Realm mRealm = Realm.getDefaultInstance();

        RealmResults<ChatItem> list = mRealm.where(ChatItem.class).findAll();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(item.getName())) return;
        }

        mRealm.beginTransaction();
        ChatItem cItem = mRealm.createObject(ChatItem.class, item.getName());
        cItem.setIconResId(item.getIconResId());
        cItem.setContents(item.getContents());

        mRealm.commitTransaction();
    }

    void selectInterest(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.filter_1:
                intersResult = "맛집";
                break;
            case R.id.filter_2:
                intersResult = "사진";
                break;
            case R.id.filter_3:
                intersResult = "드라이브";
                break;
            case R.id.filter_4:
                intersResult = "대화";
                break;
            case R.id.filter_5:
                intersResult = "카페";
                break;
            case R.id.filter_6:
                intersResult = "액티비티";
                break;
            case R.id.filter_7:
                intersResult = "등산";
                break;
            case R.id.filter_8:
                intersResult = "명상";
                break;
            case R.id.filter_9:
                intersResult = "관광";
                break;

        }
    }
}
