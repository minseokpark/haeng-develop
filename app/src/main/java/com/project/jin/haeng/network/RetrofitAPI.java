package com.project.jin.haeng.network;

import android.util.Log;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by minseok on 09/10/2018.
 * haeng-develop.
 */
public class RetrofitAPI {

    private static RetrofitAPI instance = new RetrofitAPI();

    public static RetrofitAPI getInstance() { return instance; }

    private Retrofit getNaverRetrofit() {
        NetworkInterceptor interceptor = new NetworkInterceptor();
        interceptor.setLevel(NetworkInterceptor.Level.BODY);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://openapi.naver.com/v1/")
                .client(getUnsafeOkHttpClient(interceptor))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    private Retrofit getHaengRetrofit() {
        NetworkInterceptor interceptor = new NetworkInterceptor();
        interceptor.setLevel(NetworkInterceptor.Level.BODY);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ksw0154.cafe24.com/")
                .client(getUnsafeOkHttpClient(interceptor))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


    public <S> S createNaverService(Class<S> serviceClass) { return getNaverRetrofit().create(serviceClass); }

    public <S> S createHaengService(Class<S> serviceClass) { return getHaengRetrofit().create(serviceClass); }


    public static OkHttpClient getUnsafeOkHttpClient(NetworkInterceptor interceptor) {
        Log.d("RetrofitAPITrace", "Start getUnsafeOkHttpClient");

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .addInterceptor(interceptor)
                    .retryOnConnectionFailure(true)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();

            Log.d("RetrofitAPITrace", "END getUnsafeOkHttpClient");

            return okHttpClient;

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

