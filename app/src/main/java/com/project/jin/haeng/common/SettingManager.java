package com.project.jin.haeng.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

/**
 * Created by minseok on 2018. 8. 15..
 * haeng-develop.
 */
public class SettingManager {

    private static final String SUFFIX_SHAREDPREFERENCE_KEY = "HaengSharedPreference";

    public static final String KEY_USER_ID = "__key_user_id_";
    public static final String KEY_USER_NAME = "__key_user_name_";
    public static final String KEY_USER_AGE = "__key_user_age_";
    public static final String KEY_USER_SEX = "__key_user_sex_";
    public static final String KEY_USER_EMAIL = "__key_user_email_";
    public static final String KEY_USER_IMAGE= "__key_user_image_";
    public static final String KEY_USER_BIRTH = "__key_user_birth_";
    public static final String KEY_USER_ADDRESS = "__key_user_address_";
    public static final String KEY_USER_INTRODUCE = "__key_user_introduce_";
    public static final String KEY_USER_NAVER_AUTH_CODE = "__key_user_naver_auth_code_";
    public static final String KEY_USER_NAVER_ACCESS_TOKEN = "__key_user_naver_access_token";


    Context mContext;

    static boolean isLoaded = false;

    /* 미리 데이터를 담아둘 변수를 만들어두지 않으면, 호출할떄마다 Context를 같이 불러와야하는 구나.  */
    String userId;
    String userName;
    String userAge;
    String userSex;
    String userImage;
    String userEmail;
    String userBirth;
    String address;
    String introduce;
    String naverAuthCode;
    String naverAccessToken;

    public SettingManager(Context mContext) {
        this.mContext = mContext;
    }

    public void loadSettingValue() {
        SharedPreferences mSharedPref = getSharedPreference(mContext);
        if (mContext != null && mSharedPref != null) {
            this.userId = mSharedPref.getString(KEY_USER_ID, "");
            this.userName = mSharedPref.getString(KEY_USER_NAME, "");
            this.userAge = mSharedPref.getString(KEY_USER_AGE, "");
            this.userSex = mSharedPref.getString(KEY_USER_SEX, "");
            this.userEmail = mSharedPref.getString(KEY_USER_EMAIL, "");
            this.userImage = mSharedPref.getString(KEY_USER_IMAGE, "");
            this.userBirth = mSharedPref.getString(KEY_USER_BIRTH, "");
            this.address = mSharedPref.getString(KEY_USER_ADDRESS, "");
            this.introduce = mSharedPref.getString(KEY_USER_INTRODUCE, "");
            this.naverAuthCode = mSharedPref.getString(KEY_USER_NAVER_AUTH_CODE, "");
            this.naverAccessToken = mSharedPref.getString(KEY_USER_NAVER_ACCESS_TOKEN, "");
        }
    }

    public String getUserName() { return userName; }
    public boolean setUserName(String userName) {
        this.userName = userName;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_NAME, userName)
                .commit();
    }

    public String getUserId() { return userId; }
    public boolean setUserId(String userId) {
        this.userId = userId;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_ID, userId)
                .commit();
    }

    public String getUserAge() { return userAge; }
    public boolean setUserAge(String userAge) {
        this.userAge = userAge;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_AGE, userAge)
                .commit();
    }

    public String getUserSex() { return userSex; }
    public boolean setUserSex(String userSex) {
        this.userSex = userSex;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_SEX, userSex)
                .commit();
    }

    public String getUserBirth() { return userBirth; }
    public boolean setUserBirth(String userBirth) {
        this.userBirth = userBirth;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_BIRTH, userBirth)
                .commit();
    }

    public String getUserImage() { return userImage; }
    public boolean setUserImage(String userImage) {
        this.userImage = userImage;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_IMAGE, userImage)
                .commit();
    }

    public String getUserEmail() { return userEmail; }
    public boolean setUserEmail(String userEmail) {
        this.userEmail = userEmail;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_EMAIL, userEmail)
                .commit();
    }

    public String getUserAddress() { return address; }
    public boolean setUserAddress(String address) {
        this.address = address;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_ADDRESS, address)
                .commit();
    }

    public String getUserIntroduce() { return introduce; }

    public boolean setUserIntroduce(String introduce) {
        this.introduce = introduce;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_INTRODUCE, introduce)
                .commit();
    }

    public String getNaverAuthCode() { return naverAuthCode; }
    public boolean setNaverAuthCode(String naverAuthCode) {
        this.naverAuthCode = naverAuthCode;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_NAVER_AUTH_CODE, naverAuthCode)
                .commit();
    }

    public String getNaverAccessToken() { return naverAccessToken; }
    public boolean setNaverAccessToken(String naverAccessToken) {
        this.naverAccessToken = naverAccessToken;

        return getSharedPreference(mContext).edit()
                .putString(KEY_USER_NAVER_ACCESS_TOKEN, naverAccessToken)
                .commit();
    }

    private static SharedPreferences getSharedPreference(@NonNull Context context) {
        return context.getSharedPreferences(getPreferenceName(context), Context.MODE_PRIVATE);
    }

    private static String getPreferenceName(@NonNull Context context) {
        return String.format("%s.%s", context.getPackageName(), SUFFIX_SHAREDPREFERENCE_KEY);
    }
}
