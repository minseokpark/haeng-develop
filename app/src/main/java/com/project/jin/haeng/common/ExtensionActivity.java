package com.project.jin.haeng.common;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import io.realm.Realm;

/**
 * Created by minseok on 2018. 8. 15..
 * haeng-develop.
 */
public class ExtensionActivity extends AppCompatActivity {
    private ProgressDialog progressDialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        Realm.init(this);

        createProgress();
    }

    private void createProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
    }

    /**
     * 프로그레스 시작
     */
    public void startProgress() {
        Message msg = new Message();
        progressHandler.sendMessage(msg);
    }

    /**
     * 프로그레스 종료
     */
    public void endProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    public Handler progressHandler = new Handler(msg -> {
        if (progressDialog != null) {
            try {
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable((Color.TRANSPARENT)));
                progressDialog.setTitle("불러오고 있습니다.");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();

            } catch (Exception e) { }
        }

        return false;
    });

    public void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
