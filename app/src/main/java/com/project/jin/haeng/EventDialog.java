package com.project.jin.haeng;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.project.jin.haeng.common.GlobalUtils;

public class EventDialog extends Dialog {
    private LinearLayout layoutContent;
    private ImageView imgPopup;


    private String url;
    private Context context;

    public EventDialog(Context context, String url) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        this.context = context;
        this.url = url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_image);

        getWindow().setLayout(android.view.WindowManager.LayoutParams.MATCH_PARENT, android.view.WindowManager.LayoutParams.MATCH_PARENT);

        layoutContent = (LinearLayout) findViewById(R.id.layout_content);
        layoutContent.setOnClickListener(view -> dismiss());
        imgPopup = (ImageView) layoutContent.findViewById(R.id.img_popup);


        Glide.with(context)
                .load(GlobalUtils.getServerUrl() + url)
                .into(imgPopup);
    }

    @Override
    public void onBackPressed() {}
}
