package com.project.jin.haeng;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.google.gson.JsonObject;
import com.project.jin.haeng.common.ExtensionActivity;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.common.SettingManager;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jin on 2018-06-10.
 */

public class NewCompanyActivity extends ExtensionActivity {

    EditText editDestination;
    EditText editComment;
    Button btnSchedule;
    Button btnFilter;
    Button btnSave;

    RadioButton filter_1;
    RadioButton filter_2;
    RadioButton filter_3;
    RadioButton filter_4;
    RadioButton filter_5;
    RadioButton filter_6;
    RadioButton filter_7;
    RadioButton filter_8;
    RadioButton filter_9;

    String intersResult = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_company);

        editDestination = findViewById(R.id.edit_destination);
        editComment = findViewById(R.id.edit_content);

        btnSchedule = findViewById(R.id.btn_schedule);
        btnFilter = findViewById(R.id.btn_filter);

        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(view -> {
            SettingManager setting = HaengApplication.getSettingManager();

            String context = editComment.getText().toString();
            String destination = editDestination.getText().toString();
            String rawDate = btnSchedule.getText().toString();

            String[] dates = rawDate.split("-");

//            UID, DESTINATION, CONTEXT, STARTDATE, ENDDATE, INTREST
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("UID", setting.getUserId());
            jsonObject.addProperty("NAME", setting.getUserName());
            jsonObject.addProperty("DESTINATION", destination);
            jsonObject.addProperty("CONTEXT", context);
            jsonObject.addProperty("STARTDATE", dates[0]);
            jsonObject.addProperty("ENDDATE", dates[1]);
            jsonObject.addProperty("INTREST", intersResult);

            RetrofitAPI.getInstance().createHaengService(Request.class)
                    .addUserBoard(jsonObject)
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            finish();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            finish();
                        }
                    });
        });

        btnSchedule.setOnClickListener(view -> {
            DatePickerDialog startDate;
            final DatePickerDialog endDate;

            Calendar today = Calendar.getInstance();
            int todayMonth = today.get(Calendar.MONTH);
            int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
            int todayYear = today.get(Calendar.YEAR);

            endDate = new DatePickerDialog(NewCompanyActivity.this,
                    (view1, year, month, day) -> {
                        Calendar c = Calendar.getInstance();
                        c.set(year, month, day);

                        String departmentDate = btnSchedule.getText().toString();
                        try {
                            String arriveDate = new SimpleDateFormat("yyyy.MM.dd").format(c.getTime());

                            Date departDate = new SimpleDateFormat("yyyy.MM.dd-").parse(departmentDate);
                            Date arrive = new SimpleDateFormat("yyyy.MM.dd").parse(arriveDate);

                            if (departDate.before(arrive)) {
                                btnSchedule.setText(departmentDate + arriveDate);
                            } else {
                                toast("기간 설정이 잘못되었습니다.");
                                btnSchedule.setText("일정을 입력하세요");
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }, todayYear, todayMonth, todayDayOfMonth);
            endDate.setTitle("도착 일정");
            endDate.setCancelable(false);

            startDate = new DatePickerDialog(NewCompanyActivity.this,
                    (view12, year, month, day) -> {
                        Calendar c = Calendar.getInstance();
                        c.set(year, month, day);
                        String departmentDate = new SimpleDateFormat("yyyy.MM.dd-").format(c.getTime());

                        btnSchedule.setText(departmentDate);
                        endDate.show();
                    }, todayYear, todayMonth, todayDayOfMonth);
            startDate.setTitle("출발 일정");
            startDate.setCancelable(false);
            startDate.show();
        });

        btnFilter.setOnClickListener(view ->

        {
            View dlgView = View.inflate(this, R.layout.dialog_filter, null);
            filter_1 = dlgView.findViewById(R.id.filter_1);
            filter_1.setOnClickListener(view1 -> selectInterest(view1));
            filter_2 = dlgView.findViewById(R.id.filter_2);
            filter_2.setOnClickListener(view2 -> selectInterest(view2));
            filter_3 = dlgView.findViewById(R.id.filter_3);
            filter_3.setOnClickListener(view3 -> selectInterest(view3));
            filter_4 = dlgView.findViewById(R.id.filter_4);
            filter_4.setOnClickListener(view4 -> selectInterest(view4));
            filter_5 = dlgView.findViewById(R.id.filter_5);
            filter_5.setOnClickListener(view5 -> selectInterest(view5));
            filter_6 = dlgView.findViewById(R.id.filter_6);
            filter_6.setOnClickListener(view6 -> selectInterest(view6));
            filter_7 = dlgView.findViewById(R.id.filter_7);
            filter_7.setOnClickListener(view7 -> selectInterest(view7));
            filter_8 = dlgView.findViewById(R.id.filter_8);
            filter_8.setOnClickListener(view8 -> selectInterest(view8));
            filter_9 = dlgView.findViewById(R.id.filter_9);
            filter_9.setOnClickListener(view9 -> selectInterest(view9));
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setTitle("관심사 선택");
            dlg.setView(dlgView);
            dlg.setPositiveButton("확인", (dialogInterface, i) -> {
            });
            dlg.setNegativeButton("취소", null);
            dlg.show();
        });
    }

    void selectInterest(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.filter_1:
                intersResult = "맛집";
                break;
            case R.id.filter_2:
                intersResult = "사진";
                break;
            case R.id.filter_3:
                intersResult = "드라이브";
                break;
            case R.id.filter_4:
                intersResult = "대화";
                break;
            case R.id.filter_5:
                intersResult = "카페";
                break;
            case R.id.filter_6:
                intersResult = "액티비티";
                break;
            case R.id.filter_7:
                intersResult = "등산";
                break;
            case R.id.filter_8:
                intersResult = "명상";
                break;
            case R.id.filter_9:
                intersResult = "관광";
                break;

        }
    }
}
