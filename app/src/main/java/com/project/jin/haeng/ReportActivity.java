package com.project.jin.haeng;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.project.jin.haeng.common.ExtensionActivity;

/**
 * Created by jin on 2018-06-10.
 */

public class ReportActivity extends ExtensionActivity {
    Button reportSave, reportCancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        reportSave = (Button)findViewById(R.id.reportSave);
        reportCancel = (Button)findViewById(R.id.reportCancel);

        reportSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(),PersonalActivity.class);
//                startActivity(intent);
                finish();
            }
        });
        reportCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(),PersonalActivity.class);
//                startActivity(intent);
                finish();
            }
        });
    }
}
