package com.project.jin.haeng.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.jin.haeng.R;
import com.project.jin.haeng.data.ChatItem;
import com.project.jin.haeng.data.SearchItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by jin on 2018-05-24.
 */

public class SearchAdapter extends BaseAdapter {

    private ArrayList<SearchItem> mOriginalItems = new ArrayList<>();
    private ArrayList<SearchItem> mItems = new ArrayList<>();
    private Context mContext;
    private MainListCallback mClickToChat;
    private boolean onlyView = false;

    public SearchAdapter(Context context, MainListCallback onClickChat) {
        mContext = context;
        mClickToChat = onClickChat;
    }

    public SearchAdapter(Context context, MainListCallback onClickChat, boolean onlyView) {
        mContext = context;
        mClickToChat = onClickChat;
        this.onlyView = onlyView;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public SearchItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context = parent.getContext();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_find_company, parent, false);
        }

        ImageView iv_img = (ImageView) convertView.findViewById(R.id.iv_img);
        TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tv_location = (TextView) convertView.findViewById(R.id.tv_location);
        TextView tv_period = (TextView) convertView.findViewById(R.id.tv_period);
        TextView tv_contents = (TextView) convertView.findViewById(R.id.tv_contents);
        TextView tv_interest = convertView.findViewById(R.id.tv_interest);
        Button btn_chat = (Button) convertView.findViewById(R.id.btn_chat);

        final SearchItem myItem = getItem(position);

        /* 각 위젯에 세팅된 아이템을 뿌려준다 */
        if (myItem.getUserImg() == null) {
            iv_img.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.boy));
        } else {
            Glide.with(context).load(myItem.getUserImg()).into(iv_img);
        }

        tv_name.setText(myItem.getName());
        tv_location.setText(myItem.getLocation());
        tv_period.setText(myItem.getPeriod());
        tv_contents.setText(myItem.getContents());
        tv_interest.setText(myItem.getInterest());

        btn_chat.setOnClickListener(view -> {
            ChatItem item = new ChatItem();
            item.setName(myItem.getName());
            item.setContents("");
            item.setIconResId(myItem.getIconResId());

            mClickToChat.onClickToChat(item);
        });

        if (onlyView) {
            btn_chat.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void filterDestination(String destination) {
        if (destination.isEmpty()) {
            mItems.clear();
            mItems.addAll(mOriginalItems);

            return;
        }

        ArrayList resultList = new ArrayList<SearchItem>();
        ArrayList<SearchItem> tempList = new ArrayList<>(mItems);

        for (SearchItem item : tempList) {
            if (item.getLocation().contains(destination)) resultList.add(item);
        }

        mItems.addAll(resultList);
        notifyDataSetChanged();
    }

    public void filterCategory(String interest) {
        ArrayList<SearchItem> newItmes = new ArrayList<>();
        newItmes.addAll(mItems);

        ArrayList<SearchItem> categorys = new ArrayList<>();
        for (SearchItem item : newItmes) {
            if (item.getInterest().equals(interest)) {
                categorys.add(item);
            }
        }

        mItems.clear();
        mItems.addAll(categorys);
        notifyDataSetChanged();
    }

    /**
     * @param dateStr 의 기간과 겹치는 여행만 띄어준다.
     */
    public void filterSchedule(String dateStr) {
        String[] dates = dateStr.split("-");

        Date startDate = null;
        Date endDate = null;
        try {
            startDate = new SimpleDateFormat("yyyy.MM.dd").parse(dates[0]);
            endDate = new SimpleDateFormat("yyyy.MM.dd").parse(dates[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // 중복을 방지.
        mOriginalItems.addAll(mItems);
        HashSet changedList = new HashSet<SearchItem>();
        changedList.addAll(mOriginalItems);

        // 필터 시간과 아이템 시간을 비교한다.
        for (SearchItem item : mOriginalItems) {
            String targetDate = item.getPeriod();
            String[] targetDates = targetDate.split(" - ");

            Date targetStart = null;
            Date targetEnd = null;
            try {
                targetStart = new SimpleDateFormat("yyyy-MM-dd").parse(targetDates[0]);
                targetEnd = new SimpleDateFormat("yyyy-MM-dd").parse(targetDates[1]);
            } catch (ParseException e) {
                e.printStackTrace();
            }

//            TS < SD && TE < SD
//            TS > ED && TE > ED
            if (targetStart.before(startDate) && targetEnd.before(startDate)) {
                changedList.remove(item);
                continue;
            }

            if (targetStart.after(endDate) && targetEnd.after(endDate)) {
                changedList.remove(item);
                continue;
            }
        }

        mItems.clear();
        mItems.addAll(changedList);
        notifyDataSetChanged();
    }

    /**
     * @param target   기준 대상
     * @param compared 비교할 대상
     * @return target가 compared 보다 크면 true 아니면 false
     */
    private boolean isOver(long target, long compared) {
        return target < compared;
    }

    public void addItem(SearchItem item) {
        mItems.add(item);
    }

    public void addAll(List<SearchItem> items) {
        mOriginalItems.addAll(items);
        mItems.addAll(items);
    }

    public interface MainListCallback {
        void onClickToChat(ChatItem item);
    }
}