package com.project.jin.haeng.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.project.jin.haeng.EditProfileActivity;
import com.project.jin.haeng.LoginActivity;
import com.project.jin.haeng.R;
import com.project.jin.haeng.ReportActivity;
import com.project.jin.haeng.adapter.SearchAdapter;
import com.project.jin.haeng.common.ExtensionFragment;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.common.SettingManager;
import com.project.jin.haeng.data.ImageItem;
import com.project.jin.haeng.data.SearchItem;
import com.project.jin.haeng.network.Request;
import com.project.jin.haeng.network.RetrofitAPI;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPersonalFragment extends ExtensionFragment {
    ImageButton btnReport;
    Button btnModify;
    ImageButton btnLogout;
    TextView uName, uAge, uSex, uLocation, uIntro;
    ImageView imgProfile;
    ListView comList;
    RecyclerView picList;
    View dialogView;
    EditText dlgLocation, dlgIntro;
    LinearLayout drawer_layout;
    ListView lv_activity_main_nav_list;

    LinearLayout containerProfile;

    SearchAdapter mMyAdapter;
    MainPictureFragment.PicturesAdapter picturesAdapter;

    private String[] navItems = {"프로필 수정", "신고하기", "버전정보","개발자 정보"};

    LinearLayout mContainer;


    public MainPersonalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (LinearLayout) inflater.inflate(R.layout.fragment_my_page, container, false);

            comList = (ListView) mContainer.findViewById(R.id.comList);
            btnReport = (ImageButton) mContainer.findViewById(R.id.btnReport);
            btnLogout = mContainer.findViewById(R.id.btnLogout);
            picList = mContainer.findViewById(R.id.picList);
            containerProfile = mContainer.findViewById(R.id.container_profile);

            imgProfile = mContainer.findViewById(R.id.img_profile);

            uName = mContainer.findViewById(R.id.uName);
            uAge = mContainer.findViewById(R.id.uAge);
            uSex = mContainer.findViewById(R.id.uSex);
            uLocation = mContainer.findViewById(R.id.uLocation);
            uIntro = mContainer.findViewById(R.id.uIntro);

            init();
        }

        return mContainer;
    }

    private void init() {
        setUserProfile();

        btnLogout.setOnClickListener(view -> {
            SettingManager setting = HaengApplication.getSettingManager();
            setting.setUserId("");

            toast("성공적으로 로그아웃 되었습니다.");

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        });

        containerProfile.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            startActivity(intent);
        });


        setAdatper();



//        lv_activity_main_nav_list.setAdapter( new ArrayAdapter<String>(this, R.layout.simple_list_item_1, navItems));
//        lv_activity_main_nav_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                switch (i){
//                    case 0:
//                        Intent intent = new Intent(getContext(), EditProfileActivity.class);
//                        startActivity(intent);
//                        drawer_layout.closeDrawer(lv_activity_main_nav_list);
//                        break;
//                    case 1:
//                        Intent intent1 = new Intent(getContext(), ReportActivity.class);
//                        startActivity(intent1);
//                        drawer_layout.closeDrawer(lv_activity_main_nav_list);
//                        break;
///*                    case 2:
//                        Intent intent2 = new Intent(getApplicationContext(), ReportActivity.class);
//                        startActivity(intent2);
//                        drawer_layout.closeDrawer(lv_activity_main_nav_list);
//                        break;
//                    case 3:
//                        Intent intent3 = new Intent(getApplicationContext(), drink.class);
//                        startActivity(intent3);
//                        drawer_layout.closeDrawer(lv_activity_main_nav_list);
//                        break;*/
//
//                }
//            }
//        });

        btnReport.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ReportActivity.class);
            startActivity(intent);
        });
    }

    private void setAdatper() {
        SettingManager setting = HaengApplication.getSettingManager();

        JsonObject boardJson = new JsonObject();
        boardJson.addProperty("UID", setting.getUserId());
        boardJson.addProperty("NAME", setting.getUserName());
        RetrofitAPI.getInstance().createHaengService(Request.class)
                .getMyBoard(boardJson)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        mMyAdapter = new SearchAdapter(getActivity(), item -> { }, true);
                        comList.setAdapter(mMyAdapter);
                        mMyAdapter.notifyDataSetChanged();

                        ArrayList<SearchItem> list = new ArrayList<>();
                        JsonArray jsonArray = response.body().get("response").getAsJsonArray();

                        for (JsonElement je : jsonArray) {
                            JsonObject post = je.getAsJsonObject();

                            SearchItem item = new SearchItem();
                            item.setContents(post.get("CONTEXT").getAsString());
                            item.setName(post.get("NAME").getAsString());
                            item.setUid(post.get("UID").getAsString());
                            item.setName(post.get("NAME").getAsString());
                            item.setLocation(post.get("DESTINATION").getAsString());
                            item.setInterest(post.get("INTREST").getAsString());

                            String period = post.get("STARTDATE").getAsString()
                                    + " - " + post.get("ENDDATE").getAsString();
                            item.setPeriod(period);

                            String rawInt = post.get("NO").getAsString();
                            int index = Integer.parseInt(rawInt);
                            item.setId(index);

                            list.add(item);
                        }

                        mMyAdapter.addAll(list);
                        mMyAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });

        RetrofitAPI.getInstance().createHaengService(Request.class)
                .getMyPictures(boardJson)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        picturesAdapter = new MainPictureFragment.PicturesAdapter(getActivity(), new ArrayList<>(), new MainPictureFragment.PictureCallback() {
                            @Override
                            public void onClickPicture(ImageItem item) {

                            }
                        });
                        picList.setAdapter(picturesAdapter);
                        picList.setLayoutManager(new LinearLayoutManager(getActivity()));

                        ArrayList<ImageItem> imageList = new ArrayList<>();
                        JsonArray photos = response.body().get("response").getAsJsonArray();
                        for (JsonElement item : photos) {
                            JsonObject json = item.getAsJsonObject();

                            ImageItem img = new ImageItem();
                            img.index = json.get("NO").getAsInt();
                            img.userId = json.get("UID").getAsString();
                            img.userName = json.get("NAME").getAsString();
                            img.isEvent = json.get("EVENT").getAsInt() == 1;
                            img.content = json.get("CONTENTS").getAsString();
                            img.location = json.get("LOCATION").getAsString();
                            img.date = json.get("UPLOAD").getAsString();
                            img.remoteUrl = json.get("FILENAME").getAsString();

                            imageList.add(img);
                        }

                        picturesAdapter.refreshList(imageList);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });



    }

    /*    public boolean onCreateOptionsMenu(Menu menu){
        PersonalActivity.super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.set_menu,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.edtProfile:
                dialogView = (View)View.inflate(PersonalActivity.this,R.layout.dlg_profile,null);
                AlertDialog.Builder dlg = new AlertDialog.Builder(PersonalActivity.this);
                dlg.setTitle("자기소개 수정");
                dlg.setView(dialogView);
                dlg.setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dlgLocation = (EditText)dialogView.findViewById(R.id.dlgLocation);
                                dlgIntro = (EditText)dialogView.findViewById(R.id.dlgIntro);

                                uLocation.setText(dlgLocation.getText().toString());
                                uIntro.setText(dlgIntro.getText().toString());
                            }
                        });
                dlg.setNegativeButton("취소",null);
                dlg.show();
                return true;
            case R.id.activity_report:
                Intent intent = new Intent(getApplicationContext(),ReportActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }*/


    /*    public boolean onCreateOptionsMenu(Menu menu){
        PersonalActivity.super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.set_menu,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.edtProfile:
                dialogView = (View)View.inflate(PersonalActivity.this,R.layout.dlg_profile,null);
                AlertDialog.Builder dlg = new AlertDialog.Builder(PersonalActivity.this);
                dlg.setTitle("자기소개 수정");
                dlg.setView(dialogView);
                dlg.setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dlgLocation = (EditText)dialogView.findViewById(R.id.dlgLocation);
                                dlgIntro = (EditText)dialogView.findViewById(R.id.dlgIntro);

                                uLocation.setText(dlgLocation.getText().toString());
                                uIntro.setText(dlgIntro.getText().toString());
                            }
                        });
                dlg.setNegativeButton("취소",null);
                dlg.show();
                return true;
            case R.id.activity_report:
                Intent intent = new Intent(getApplicationContext(),ReportActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }*/


    private void setUserProfile() {
        SettingManager setting = HaengApplication.getSettingManager();
        uName.setText(setting.getUserName());
        uAge.setText(setting.getUserAge());
        uSex.setText(setting.getUserSex());
        uLocation.setText(setting.getUserAddress());
        uIntro.setText(setting.getUserIntroduce());

        Glide.with(getActivity()).load(setting.getUserImage()).into(imgProfile);
    }
}
