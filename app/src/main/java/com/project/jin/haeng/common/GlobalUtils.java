package com.project.jin.haeng.common;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by minseok on 09/10/2018.
 * haeng-develop.
 */
public class GlobalUtils {
    public static int pxTodp(int size) {
        Resources r = Resources.getSystem();
        float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, r.getDisplayMetrics());

        return (int) dp;
    }

    public static int pxTodp(float size) {
        Resources r = Resources.getSystem();
        float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, r.getDisplayMetrics());

        return (int) dp;
    }

    public static int dpTopx(int dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static String getServerUrl() {
        return "http://ksw0154.cafe24.com/";
    }

//        return "pic3.jpg";
    public static String getImageUrl() {
        return getServerUrl() + "gallery_image/";
    }
}
