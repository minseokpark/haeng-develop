package com.project.jin.haeng.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minseok on 2018. 8. 16..
 * haeng-develop.
 */
public class MessageItem extends RealmObject {
    @PrimaryKey
    int id;
    public String msg;
    public String sender;
    public String time;
}
