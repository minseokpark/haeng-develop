package com.project.jin.haeng.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.project.jin.haeng.ChatroomActivity;
import com.project.jin.haeng.R;
import com.project.jin.haeng.adapter.ChatListAdapter;
import com.project.jin.haeng.adapter.ChattingAdapterView;
import com.project.jin.haeng.common.ExtensionFragment;
import com.project.jin.haeng.data.ChatItem;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainChatListFragment extends ExtensionFragment implements ChattingAdapterView {

    private LinearLayout mContainer;
    private ListView cListView;
    private Button btn;

    ChatListAdapter mMyAdapter;

    public MainChatListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (LinearLayout) inflater.inflate(R.layout.fragment_chat_list, container, false);

            cListView = mContainer.findViewById(R.id.listView);
        }

        dataSetting();

        return mContainer;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void dataSetting(){
        mMyAdapter = new ChatListAdapter(getActivity(), this);


        /*for (int i=0; i<10; i++) {
            mMyAdapter.addItem(ContextCompat.getDrawable(getApplicationContext(), R.drawable.girl), "name_" + i, "contents_" + i);
        }*/
//        mMyAdapter.addItem(ContextCompat.getDrawable(getContext(), R.drawable.girl), "김이연", "여행가요~!");
//        mMyAdapter.addItem(ContextCompat.getDrawable(getContext(), R.drawable.boy), "김상원", "여행 언제간다구요?");

        RealmResults<ChatItem> tempChatList = Realm.getDefaultInstance()
                .where(ChatItem.class)
                .findAll();

        ArrayList<ChatItem> chatList = new ArrayList<>();
        chatList.addAll(tempChatList);

        mMyAdapter.addItems(tempChatList);

        /* 리스트뷰에 어댑터 등록 */
        cListView.setAdapter(mMyAdapter);
    }

    @Override
    public void enterChatting(ChatItem item) {
        Intent intent = new Intent(getContext(), ChatroomActivity.class);
        intent.putExtra("name", item.getName());
        startActivity(intent);
    }
}
