package com.project.jin.haeng;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.project.jin.haeng.common.ExtensionActivity;
import com.project.jin.haeng.data.ImageItem;
import com.project.jin.haeng.fragment.MainChatListFragment;
import com.project.jin.haeng.fragment.MainListFragment;
import com.project.jin.haeng.fragment.MainPersonalFragment;
import com.project.jin.haeng.fragment.MainPictureFragment;

public class MainActivity extends ExtensionActivity {
    static public ImageButton btnSearch, btnChat, btnPic, btnPersonal, btnDetail;
    FrameLayout mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

   /* 위젯과 멤버변수 참조 획득 */

        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        btnChat = (ImageButton)findViewById(R.id.btnChat);
        btnPic = (ImageButton)findViewById(R.id.btnPic);
        btnPersonal = (ImageButton)findViewById(R.id.btnPersonal);

        mainContainer = (FrameLayout) findViewById(R.id.main_content);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_content, new MainListFragment())
                .commit();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_content, new MainListFragment())
                        .commit();
            }
        });

        btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_content, new MainChatListFragment())
                        .commit();
            }
        });
        btnPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_content, new MainPictureFragment())
                        .commit();
            }
        });
        btnPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_content, new MainPersonalFragment())
                        .commit();
            }
        });
//        btnDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDetailPicture();
//            }
//        });
    }

    public void showDetailPicture(ImageItem item) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_content, DetailPictureFragment.getInstance(item))
                .commit();
    }

    @Override
    public void onBackPressed() {

        String currentState = getSupportFragmentManager()
                .findFragmentById(R.id.main_content)
                .getClass().getSimpleName();

        // 이미지 상세보기의 경우 이미지 리스트로 돌아간다.
        if (currentState.equals("DetailPictureFragment")) {
            btnPic.performClick();
        } else {
            super.onBackPressed();
        }
    }
}