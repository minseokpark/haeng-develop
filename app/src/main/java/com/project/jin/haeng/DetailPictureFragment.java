package com.project.jin.haeng;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.project.jin.haeng.common.ExtensionFragment;
import com.project.jin.haeng.common.GlobalUtils;
import com.project.jin.haeng.data.ImageItem;

public class DetailPictureFragment extends ExtensionFragment {
    static public DetailPictureFragment INSTANCE = null;

    ConstraintLayout mContainer;
    static ImageItem mItem;

    TextView txtName;
    ImageView imgProfile;
    TextView txtLocation;
    TextView txtDate;
    TextView txtContent;

    public DetailPictureFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (ConstraintLayout) inflater.inflate(R.layout.fragment_detail_picture, container, false);

            txtName = mContainer.findViewById(R.id.txt_name);
            imgProfile = mContainer.findViewById(R.id.img_profile);
            txtLocation = mContainer.findViewById(R.id.txt_location);
            txtDate = mContainer.findViewById(R.id.txt_date);
            txtContent = mContainer.findViewById(R.id.txt_content);

        }

        initItem();

        return mContainer;
    }

    private void initItem() {
        ImageView imageView = mContainer.findViewById(R.id.img_picture);
        Glide.with(getActivity()).load(GlobalUtils.getServerUrl() + mItem.remoteUrl).into(imageView);
        txtName.setText(mItem.userName);
        txtLocation.setText(mItem.location);
        txtDate.setText(mItem.date);
        txtContent.setText(mItem.content);

        Glide.with(getActivity()).load(mItem.userImg).into(imgProfile);
    }

    static public DetailPictureFragment getInstance(ImageItem item) {
        if (INSTANCE == null) {
            INSTANCE = new DetailPictureFragment();
        }
        mItem = item;

        return INSTANCE;
    }
}
