package com.project.jin.haeng;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.project.jin.haeng.adapter.ChattingAdapter;
import com.project.jin.haeng.common.HaengApplication;
import com.project.jin.haeng.data.MessageItem;

public class ChatroomActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    ChattingAdapter mAdapter;

    TextView mTitle;

    EditText editMessage;
    Button btnBack;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom);

        mRecyclerView = findViewById(R.id.rv_chat);
        editMessage = findViewById(R.id.edit_message);
        btnBack = findViewById(R.id.btn_back);
        btnSend = findViewById(R.id.btn_send);

        btnBack.setOnClickListener(view -> finish());

        btnSend.setOnClickListener(view -> {
            String msg = editMessage.getText().toString();

            sendMessage(msg);
        });

        editMessage.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEND) {
                btnSend.performClick();
            }
            return false;
        });

        mTitle = findViewById(R.id.title_chatroom);
        mTitle.setText(getIntent().getStringExtra("name") + "님과의 대화");

        mAdapter = new ChattingAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        MessageItem item = new MessageItem();
        item.msg = "안녕하세요!";
        item.sender = getIntent().getStringExtra("name");
        item.time = "2018-10-30";
        mAdapter.addMessage(item);
    }

    void clearInputMessage() {
        editMessage.setText("");
    }

    void sendMessage(String msg) {
        MessageItem item = new MessageItem();

        item.msg = msg;
        item.sender = HaengApplication.getSettingManager().getUserName();
        item.time = "";

        mAdapter.addMessage(item);
        clearInputMessage();
    }
}
